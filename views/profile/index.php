<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Profiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Profile', ['create'], ['class' => 'btn btn-success']) ?>
    </p>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'firstName',
            'lastName',
            'phone',
            'address',
            [
                'label' => 'Public',
                'attribute' => 'isPublished',
                'filter' => ['0' => 'NO', '1' => 'Yes'],
                'filterInputOptions' => ['prompt' => 'Show Public Profiles', 'class' => 'form-control', 'id' => null],
                'value' => function($model){
                    if ($model->isPublished == true){
                        return 'Yes';
                    } else {
                        return 'NO';
                    }
                }
            ],
            [
                'attribute' => 'Image',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->image_web_address!='')
                        return '<img src="'.Yii::$app->homeUrl. 'uploads/'.$model->image_web_address.'" width="50px" height="auto">'; else return 'no image';
                },
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
