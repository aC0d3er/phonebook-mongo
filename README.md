<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 phonebook Project with mongodb</h1>
    <br>
</p>



REQUIREMENTS
------------

- The minimum requirement by this project template that your Web server supports PHP 5.4.0.
- installed mongodb locally


INSTALLATION
------------

after clone this project to your prefer directory open your commandLine on that path and then run this command
~~~
php yii create
~~~

with this command you can create a user based on commandLine.
after user created run the yii2 local server with this command

~~~
php yii serve
~~~
that's it