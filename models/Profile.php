<?php

namespace app\models;

use Yii;
use yii\mongodb\ActiveRecord;

/**
 * This is the model class for collection "profile".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $firstName
 * @property mixed $lastName
 * @property mixed $phone
 * @property mixed $address
 * @property mixed $isPublished
 * @property mixed $image_web_address
 * @property mixed $createdBy
 * @property mixed $created_by
 */
class Profile extends ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return ['yii2mongo', 'profile'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'firstName',
            'lastName',
            'phone',
            'address',
            'isPublished',
            'image_web_address',
            'created_by',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName', 'phone', 'address', 'isPublished','image', 'image_web_address', 'created_by'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, jpeg, png'],
            [['image'], 'file', 'maxSize'=>'100000000'],
            [['phone'], 'integer'],
            [['address','created_by'], 'string'],
            [['created_by'], 'required'],
            [['isPublished'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'phone' => 'Phone',
            'address' => 'Address',
            'isPublished' => 'Is Published',
            'image_web_address' => 'Image Web Address',
            'created_by' => 'Created By',
        ];
    }

    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['_id' => 'created_by']);
    }

    public function allPublic()
    {
        $public = Profile::find()->where(['isPublished' => true])->all();
    }
}
