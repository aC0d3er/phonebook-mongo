<?php


namespace app\commands;



use app\models\SignupForm;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

class CreateController extends Controller
{
    /**
     * This command creates new user account.


     * @param string $username Username
     * @param string $password Password
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {

        $username = $this->prompt(
            'Enter username ( must be at least 3 chars)',
            [
                'required' => true,
                'validator' => function ($input, &$error) {
                    if (strlen($input) < 3) {
                        $error = 'The username must be at least 3 chars !';
                        return false;
                    }
                    return true;
                },
            ]
        );
        $password =  $this->prompt(
            'Enter password ( must be at least 8 chars)',
            [
                'required' => true,
                'validator' => function ($input, &$error) {
                    if (strlen($input) < 8 ) {
                        $error = 'The password must be at least 8 chars !';
                        return false;
                    }
                    return true;
                },
            ]
        );
        $model = new SignupForm();
        $model->username = $username;
        $model->password = $password;
        if ( $model->signup()) {
            $this->stdout('User has been created' . "!\n", Console::FG_GREEN);
        } else {
            $this->stdout('Please fix following errors:' . "\n", Console::FG_RED);
            echo "A problem occurred!\n";
            return ExitCode::UNSPECIFIED_ERROR;
            }
    }
}
